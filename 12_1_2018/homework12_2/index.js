const mysql2 = require('mysql2/promise')
const User = require('./model/user')

async function main () {
  const db = await mysql2.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'hmday15'
  })

  // const user1 = await User.find(db, 1)
  // user1.firstName = 'test555'
  // await User.store(db, user1)

  // const user1 = await User.findAll(db)
  // console.log(user1)

  // const user2 = await User.findByUsername(db, "abc5")
  // user2.firstName = "1222"
  // await User.store(db,user2)

  // await User.remove(db, 8)
  
  const [row] = await db.execute(`select * from users`)
  console.log([row])
}

main()