const mysql = require('mysql2/promise')

const pool = mysql.createPool({
  host: 'localhost',
  port: '3306',
  user: 'root',
  password: 'password',
  database: 'hmday15'
})

;(async function () {
  const db = await pool.getConnection()
  
  await db.beginTransaction()
  try {
    const User = require('../model/user.js')(db)

    // const user1 = await User.find(2)
    // const user1 = await User.findByUsername("tester7")
    // user1.firstName = 'abc88888'
    // await user1.save()

    const user1 = await User.findAll()
    console.log(user1)

    // const user2 = await User.find(2)
    // await user2.remove()

    await db.commit()
    // const [rows] = await db.execute(`select id, first_name, last_name from users where id = ?`, [9])
    console.log(rows)

    // await db.execute(`update users set first_name = ? where id = ?`, ['new user', 1])


  } catch (err) {
    console.log(err)
    await db.rollback()
  }
  await db.release()
})().then(
 () => {},
 (err) => { console.log(err) }
)