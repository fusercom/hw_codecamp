peopleSalary = [
    {"id":"1001", "firstname":"Luke", "lastname":"Skywalker", "company":"Walt Disney", "salary":40000}, 
    {"id":"1002", "firstname":"Tony", "lastname":"Stark", "company":"Marvel", "salary":1000000}, 
    {"id":"1003", "firstname":"Somchai", "lastname":"Jaidee", "company":"Love2work", "salary":20000}, 
    {"id":"1004", "firstname":"Monkey D", "lastname":"Luffee", "company":"One Piece", "salary":9000000} 
]
let people = []
for (let i = 0; i < peopleSalary.length; i++) {          //loop หาจำนวนของ peopleSalary
    let obj = {}                                         // assign ค่า object ให้ obj
    for (let j in peopleSalary[i]) {                     // assign ค่า peopleSalary[i] ให้ j
        if (j != "company") {                            // ถ้า j มีค่าไม่เท่ากับ  "company"
            obj[j] = peopleSalary[i][j]                  
        }
    }
    people.push(obj)                                     // assign ค่า obj ใส่ใน people
}
// console.log(people)

let salary1 = []                                    
for (let i=0; i<people.length; i++){
    // console.log(people[i].salary)
    let total = people[i].salary
    salary1.push(total)
    for(let a=0; a<2; a++){
        total = total * 1.1
        console.log(total)
        salary1.push(total)
        // console.log(salary1)
    }
    people[i].salary = salary1
    // console.log(people[i])
}
