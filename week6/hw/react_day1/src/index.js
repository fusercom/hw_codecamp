import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import App from './App';
// import Button from '~antd/lib/button';
// import { Button } from 'antd';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
