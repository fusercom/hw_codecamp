import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './Hw1.css';
import './Hw3.css';

import { Button, Card, Form, Icon, Radio, Input, Checkbox, Carousel, DatePicker, Col, Row, Layout, Menu, Breadcrumb } from 'antd';

const FormItem = Form.Item;
const { Meta } = Card;
const { Header, Content, Footer } = Layout;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const RadioGroup = Radio.Group;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  handleChange(event) {
    this.setState({ username: event.target.value });
    console.log(this.state.username);
  }

  handleChangePassword(event) {
    this.setState({ password: event.target.value });
    console.log(this.state.password);
  }


  render() {
    return (
      <div className="App">
        <h1>Welcome to my Application</h1>
        <Card className="signup-box" title="Sign In">
          <Form className="login-form">
            <FormItem className="pad-form">
              <div className="mg-btn">
                <h3>Username :</h3>
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" value={this.state.username}
                  onChange={this.handleChange} />
                <h3>Password :</h3>
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" value={this.state.password} onChange={this.handleChangePassword} />
              </div>
              <Button type="primary" htmlType="submit" className="login-form-button">SINGIN</Button><br />
              <a className="login-form-forgot" href="">Forgot password</a><hr />
              <div>
                <h3>New User :</h3>
                <Button type="primary" htmlType="submit" className="login-form-button">SINGUP</Button>
              </div>
            </FormItem>
          </Form>
        </Card>
      </div>
    );
  }
}

// Homework2

class hm2 extends Component {
  render() {
    return (
      <div>
        <Layout className="layout">
          <Header>
            <div className="logo" />
            <Menu
              theme="dark"
              mode="horizontal"
              defaultSelectedKeys={['2']}
              style={{ lineHeight: '64px' }}
            >
              <Menu.Item key="1">Product</Menu.Item>
              <Menu.Item key="2">About</Menu.Item>
            </Menu>
          </Header>
        </Layout>

        <Carousel effect="fade">
          <div><h3>1</h3></div>
          <div><h3>2</h3></div>
          <div><h3>3</h3></div>
          <div><h3>4</h3></div>
        </Carousel>
        <div style={{ background: '#ECECEC', padding: '30px' }}>
          <Row gutter={16}>
            <Col span={6}>
              <Card title="" bordered={false} hoverable
                style={{ width: 240 }}
                cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
              >
                <Meta className="pd-card"
                  title="Europe Street beat"
                  description="www.instagram.com"
                />
              </Card>
            </Col>

            <Col span={6}>
              <Card title="" bordered={false} hoverable
                style={{ width: 240 }}
                cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
              >
                <Meta className="pd-card"
                  title="Europe Street beat"
                  description="www.instagram.com"
                />
              </Card>
            </Col>

            <Col span={6}>
              <Card title="" bordered={false} hoverable
                style={{ width: 240 }}
                cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
              >
                <Meta className="pd-card"
                  title="Europe Street beat"
                  description="www.instagram.com"
                />
              </Card>
            </Col>

            <Col span={6}>
              <Card title="" bordered={false} hoverable
                style={{ width: 240 }}
                cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
              >
                <Meta className="pd-card"
                  title="Europe Street beat"
                  description="www.instagram.com"
                />
              </Card>
            </Col>

          </Row>
        </div>

      </div>
    )
  }

}


// Homework3
class facebook extends Component {
  render() {
    return (

      <div class="container-bg">
        <div className="navbar">
          <Row gutter={16}>
            <Col span={12}>
              <div className="logo-fb">
                <h1>facebook</h1>
              </div>
            </Col>

            <Col span={12}>
              <Form layout="inline" className="box-right" onSubmit={this.handleSubmit}>
                <FormItem className="login-left">
                  <p>Email or Phone</p>
                  <input placeholder="Username" />
                </FormItem>
                <FormItem className="login-left">
                  <p>Password</p>
                  <input type="password" placeholder="Password" />
                  <p>Forgotten account?</p>
                </FormItem>
                <button className="btn-login">Log in</button>
              </Form>

            </Col>
          </Row>
        </div>

        <div className="content-box">
          <Row gutter={16}>
            <Col span={12}>
              <div className="title-p">
                {/* <h1>Recent logins</h1> */}
                <h3>Facebook helps you connect and share with<br /> the people in your life.</h3>
              </div>
              <img src="https://static.xx.fbcdn.net/rsrc.php/v3/yc/r/GwFs3_KxNjS.png" />
            </Col>

            <Col span={12} className="pd-form">
              <div className="sub-p">
                <h1>Create a new account</h1>
                <p>It's free and always will be.</p>
              </div>
              <Form onSubmit={this.handleSubmit} className="login-form" >
                <FormItem className="form-regist">
                  <div className="form-inline">
                    <Input placeholder="First name" />
                    <Input placeholder="Surname" /><br />
                  </div>
                  <div>
                    <Input type="email" placeholder="Mobile number or email address" />
                    <Input ttype="password" placeholder="New password" />
                  </div>
                </FormItem>

                <h3>Birthday</h3>
                <Form layout="inline">
                  <FormItem className="date-form">
                    <select>
                      <option>1</option>
                      <option>2</option>
                      <option>11</option>
                    </select>
                    <select>
                      <option>Jan</option>
                      <option>Feb</option>
                      <option>March</option>
                    </select>
                    <select>
                      <option>1990</option>
                      <option>1991</option>
                      <option>1992</option>
                    </select>
                    <p><a href="">Why do I need to provide<br /> my date of birth?</a></p>
                  </FormItem>
                </Form>
              
                <RadioGroup onChange={onChange} className="mb-15">
                  <Radio value={1}>Female</Radio>
                  <Radio value={2}>Male</Radio>
                </RadioGroup>
                <p>By clicking Create Account, you agree to our Terms and confirm that you have read our Data Policy, including our Cookie Use Policy. You may receive SMS message notifications from Facebook and can opt out at any time.</p>
                <div className="create-ac">
                  <button className="btn-create">Create Account</button>
                  <p><a href="#">Create a Page</a> for a celebrity, band or business.</p>
                </div>
              </Form>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

function onChange(date, dateString) {
  console.log(date, dateString);
}




// export default App;
// export default hm2;
export default facebook;
