module.exports = {
    list,
    add,
    remove,
    update,
}

async function list(db) {
    const [rows] = await db.execute(`select id,title from todo`)
    return rows
}

async function add(db, booklist) {
    let [rows] = await db.execute(`insert into todo (title, status) values (?,?)`, [booklist.title, booklist.status])
    booklist.id = rows.insertId
    if (rows.insertId) {
        return {
            id: booklist.id,
            title: booklist.title,
            status: booklist.status
        }
    }  else {
        return 'Error'
      }
}

async function remove(db, id) {
    await db.execute(`delete from todo where id = ? `, [id])
}


async function update(db, id, content) {
    return await db.execute(`update todo set title = ? where id = ?`, [content.title, id])
}