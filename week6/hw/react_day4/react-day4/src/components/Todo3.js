import React, { Component } from 'react';

import { Input, Icon, Button, Card, List ,Spin} from 'antd';

export class Todo3 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputText : '',
      listItem: [],
      isLoading: true
    }

    this.handleChangeText = this.handleChangeText.bind(this);

  }

  componentDidMount () {
    // เราควรจะ fetch เพื่อเอาค่ามาจาก MockAPI 
    this.fetchGet();
  }

  async fetchGet () {
    const result = await fetch('http://localhost:3000/todo')
    let data = await result.json();
    // console.log(data)
    let listItem = data
    // console.log(data)
    this.setState({ listItem , isLoading : false})
  }

  async fetchPost (text) {
    this.setState({isLoading : true});
    const result = await fetch('http://localhost:3000/todo', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title: text,
        status: 0
      }),
    })

    if (result.ok) {
      // ท่านี้ก็ได้ดูดีกว่า 1
      let data2 = await result.json()
      // console.log(data2)
      let listItem = this.state.listItem.concat(data2);
      // console.log(listItem)
      this.setState({ listItem , isLoading : false })

      // ท่านี้ก็ได้ดูดีกว่า 2
      //this.fetchGet();

    }
    
  }

  deleteListAtIndex = async (index, id) => {
    // ไม่ควรทำเพราะเป็นการ Render ใหม่ทั้ง State ถ้ามีเยอะก็ฉิบหายยย สิครับ
    // this.state.listItem.splice(index, 1);
    // this.setState({});
    // console.log(id)
    let data = await fetch('http://localhost:3000/todo/' + id, {
      method: 'delete'
    });
    if(data){
      const result = this.state.listItem;
      result.splice(index, 1);
      this.setState({listItem: result});
    }
    return
  }

  submitList = () => {
    this.fetchPost(this.state.inputText);
    this.setState({
      //listItem: this.state.listItem.concat([this.state.inputText]),
      inputText: ''
    })
    //console.log(this.state.listItem);
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({inputText: event.target.value});
  }

  render() {

    // const data = [
    //     'text 1',
    //     'text 2',
    //     'text 3',
    // ];

    //const { Header, Footer, Sider, Content } = Layout;
    //const Search = Input.Search;
    //const FormItem = Form.Item;

    // หลัง Return มันต้องมี DIV ครอบก่อน
    // { if 1==1 ? 'TRUE' : 'FALSE'}
    return (
        <div>
          { 
            this.state.isLoading == false ? <Card style={{ width: 500 , backgroundColor : this.props.myColor }}>
              <h1>To-do-list</h1>

              <div style={{ marginBottom:'10px'}}>
                <Input
                  addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>}
                  onChange={this.handleChangeText}
                  value={this.state.inputText}
                  onKeyPress={this.handleKeyPress}/>
              </div>
              <div className="scroll-box blue">
              <List
                bordered
                dataSource={this.state.listItem}
                renderItem={(item,index) => (
                  <List.Item actions={[<a onClick={() => this.deleteListAtIndex(index, item.id)}><Icon type="close-circle" style={{ fontSize: 16, color: 'rgb(255, 145, 0)' }} /></a>]}>
                      {item.title}
                  </List.Item>
              )}
              />
              </div>
          </Card>:<Spin />
        }
          
        </div>
      );
    }
}
