import React from 'react'
//import { Layout, Form, Input, Icon, Row, Col, Button, Card, List} from 'antd';
import './App.css';
import {Todo} from './components/Todo.js'
import {Todo2} from './components/Todo2.js'
import {Todo3} from './components/Todo3.js'

// import {Mockuplink} from './components/mockuplink.js'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

const App = () => (
  <Router>
    <div>
      <ul className="list-inline">
        <li><Link to="/">Todo1</Link></li>
        <li><Link to="/Todo2">Todo2</Link></li>
        <li><Link to="/Todo3">Todo3</Link></li>
      </ul>

      <hr/>
      <Route exact path="/" component={Todo}/>
      <Route path="/Todo2" component={Todo2}/>
      <Route path="/Todo3" component={Todo3}/>
    </div>
  </Router>
)

const Home = () => (
  <div>
    <h2>Home</h2>
  </div>
)

const About = () => (
  <div>
    <h2>About</h2>
  </div>
)

const Topics = ({ match }) => (
  <div>
    <h2>Topics</h2>
    <ul>
      <li>
        <Link to={`${match.url}/rendering`}>
          Rendering with React
        </Link>
      </li>
      <li>
        <Link to={`${match.url}/components`}>
          Components
        </Link>
      </li>
      <li>
        <Link to={`${match.url}/props-v-state`}>
          Props v. State
        </Link>
      </li>
    </ul>

    <Route path={`${match.url}/:topicId`} component={Topic}/>
    <Route exact path={match.url} render={() => (
      <h3>Please select a topic.</h3>
    )}/>
  </div>
)

const Topic = ({ match }) => (
  <div>
    <h3>{match.params.topicId}</h3>
  </div>
)

export default App