const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const cors = require('koa-cors')


const app = new Koa()
const router = require('./ctrl/route.js')
app.use(bodyParser())
app.use(cors())
app.use(router.routes())
app.listen(3000)