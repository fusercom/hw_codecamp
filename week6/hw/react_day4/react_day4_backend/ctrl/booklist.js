module.exports = function (pool, model) {
    return {
        async list(ctx) {
            ctx.body = await model.list(pool)
        },
        
        async add(ctx) {
            const todo = ctx.request.body
            const obj = await model.add(pool, todo)
            ctx.body = obj
        },

        async update(ctx) {
            const id = ctx.params.id
            const content = ctx.request.body
            const update = await model.update(pool, id, content)
            ctx.body = update
        },
        async remove(ctx) {
            const id = ctx.params.id
            ctx.body = await model.remove(pool, id)
        },
    }
}