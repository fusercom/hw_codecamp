import React, { Component } from 'react';

import './App.css';

import { Layout, Form, Input, Button, Card, List, Avatar } from 'antd';

export class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputText: '',
      listItem: [],
    }

    this.handleChangeText = this.handleChangeText.bind(this);

  }

  submitList = () => {
    let obj = {}
    let name = ""
    obj.title = name
    obj.text = this.state.inputText
    // obj.classalign = "left"
    
    


    this.setState({
      // listItem: this.state.listItem.concat([this.state.inputText]),
      listItem: this.state.listItem.concat([obj]),
      inputText: ''
    })
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({ inputText: event.target.value });
  }

  render() {


    return (
      <Card style={{ width: 500, backgroundColor: this.props.myColor }}>
        <h1>To-do-list</h1>

        <div style={{ marginBottom: '10px' }}>
          <Input
            addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>}
            onChange={this.handleChangeText}
            value={this.state.inputText}
            onKeyPress={this.handleKeyPress} />
        </div>


        <List
          itemLayout="horizontal"
          dataSource={this.state.listItem}
          renderItem={(item, index) => (
            <List.Item>
              {index % 2 == 0 ?
                <List.Item.Meta style={{textAlign:'right'}}
                  // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                  title={<a href="https://ant.design">{item.title = "Me"} {<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}</a>}
                  description={item.text}
                />
                :
                <List.Item.Meta className="text-left"
                  avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                  title={<a href="https://ant.design">{item.title = "You"} </a>}
                  description={item.text}
                />
              }
            </List.Item>
          )}
        />

      </Card>
    );
  }
}


export default (App)