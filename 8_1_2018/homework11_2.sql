use bookstore;

insert into employees (firstname, lastname, age) VALUES
	('John', 'Doe', 20),
	('Michale', 'Bay', 45),
	('William', 'Well', 25),
	('Somsri', 'Boondee', 18),
	('Chicken', 'Duck', 15),
	('Johnny', 'English', 30),
	('Dang', 'Boonchai', 50),
	('Anna', 'Caramel', 28),
	('Boonmee', 'Chokdee', 19),
	('Dave', 'David', 42);


delete from employees WHERE id = 5; 

ALTER TABLE employees
add COLUMN address VARCHAR(255);

update employees SET address = "Software Park" where id = 2;

SELECT Count(id) from employees;

select firstname, lastname, age from employees where age < 20;