show databases;

show tables;

DESCRIBE departments;
DESCRIBE dept_emp;
DESCRIBE dept_manager;
DESCRIBE employees;
DESCRIBE salaries;
DESCRIBE titles;

SELECT DISTINCT(title) from titles;

SELECT count(emp_no) as countEmployees, sum(gender = 'M') as countMale, sum(gender = 'F') as countFemale FROM employees;


SELECT count(DISTINCT(last_name)) FROM employees;