CREATE DATABASE bookstore;
use bookstore;

create table employees(
	id int auto_increment,
	firstname VARCHAR(32),
	lastname VARCHAR(32),
	age int,
	created_at TIMESTAMP DEFAULT now(),
	PRIMARY KEY(id),
	UNIQUE (firstname)
);

create table book(
	ISBN VARCHAR(32),
	name VARCHAR(255),
	price float,
	created_at TIMESTAMP DEFAULT now(),
	PRIMARY KEY(ISBN)
);

create table services(
	ISBN VARCHAR(32),
	employee_id int,
	price float,
	amount int,
	created_at VARCHAR(20),
	PRIMARY KEY(ISBN)
);





