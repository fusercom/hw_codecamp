"use strict"

const Koa = require('koa')
const session = require('koa-session')

const mysql = require('mysql2/promise')

const pool = mysql.createPool({
    host: "localhost",
    user: "root",
    password: "password",
    port: "3306",
    database: "session"
})

const sessionConfig = {
    key: 'sess',
    maxAge: 3600*1000,
    httpOnly: true,
    store: {
        async get(key, maxAge, { rolling }) {
            try {
                const [s] = await pool.execute(`select dkeys,dvalues from mysession where dkeys = ?`, [key])
                if (!s[0]) {
                    return;
                } else {
                    return JSON.parse(s[0].dvalues)
                }
            } catch(err){
                console.log(err)
            }
        },
        set(key, sess, maxAge, { rolling }) {
            try {
                const data = pool.execute(`insert into mysession (dkeys, dvalues) values (?,?) ON DUPLICATE KEY UPDATE dvalues = ?, dkeys = ?`, [key, JSON.stringify(sess), JSON.stringify(sess), key])
            } catch (err) {
                console.log(err)    
            }
            // sessionStore[key] = sess
        },
        destroy(key) {
            try {
                delete pool.execute(`delete from mysession where dkeys =?`, [key])
            } catch (error) {
                console.log(err)
            }
            // delete sessionStore[key]
        }
    }
}

const app = new Koa()

app.keys = ['supersecret']

app
    .use(session(sessionConfig, app))
    .use(handler)
    .listen(3000)


async function handler(ctx) {
    let n = ctx.session.views || 0
    ctx.session.views = ++n
    ctx.body = `${n} views`
}

