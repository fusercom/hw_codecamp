let fs = require('fs')
function jText(){
    return new Promise(function(resolve, reject){
        fs.readFile('homework1.json', 'utf8', function(err, data){
            let jsonText = JSON.parse(data)
            if (err)
                reject(err)
            else
                resolve(jsonText)
        })
    })
}


async function readFile(){
    try {
        var employees = await jText()
    } catch (error) {
        console.error(error)
    }
}
readFile();