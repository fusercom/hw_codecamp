let fs = require('fs')
function jText(){
    return new Promise(function(resolve, reject){
        fs.readFile('homework1.json', 'utf8', function(err, data){
            let jsonText = JSON.parse(data)
            if (err)
                reject(err)
            else
                resolve(jsonText)
        })
    })
}


async function readFile(){
    try {
        var employees = await jText()
        let arr = []
        let total = 0
        const name = employees.map(function(employee){
            if (employee.salary < 100000){
                employee.salary = employee.salary * 2
                arr.push(employee)
            }
            total += parseInt(employee.salary)
            return employee

        })
        console.log(name)
        console.log('Total Salary:',total)
    } catch (error) {
        console.error(error)
    }
}
readFile();