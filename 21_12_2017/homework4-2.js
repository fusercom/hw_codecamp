let fs = require('fs')
function jText(){
    return new Promise(function(resolve, reject){
        fs.readFile('homework1.json', 'utf8', function(err, data){
            let jsonText = JSON.parse(data)
            if (err)
                reject(err)
            else
                resolve(jsonText)
        })
    })
}


function allName(p1){
    p1.fullname = `${p1.firstname} ${p1.lastname}`
    return p1
}


function increaseSalary(input){
    let total = []
    let arrSalary = input.salary
    total.push(input.salary)
    total.push(Math.round(arrSalary * 1.1))
    arrSalary *= 1.1
    total.push(Math.round(arrSalary * 1.1))
    input.salary = total
    return input
}

async function readFile(){
    try {
        var employees = await jText()
        let name = employees.map(allName).map(increaseSalary)
        console.log(name)
    } catch (error) {
        console.error(error)
    }
}
readFile();




