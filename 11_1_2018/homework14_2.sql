-- drop table product;
-- drop table supplier;
-- drop table orders;
-- drop table orders_items;
-- drop table department;
-- drop table customer;
-- drop table employee;

 create table supplier(
 	id int auto_increment PRIMARY KEY,
 	name VARCHAR(255) not null,
 	address VARCHAR(255),
 	phone_number VARCHAR(20)
 );
 
 create table product(
 	id int auto_increment PRIMARY KEY,
 	supplier_id int not null,
 	name VARCHAR(255) not null,
 	description VARCHAR(255),
 	price double,	
 	quantity int DEFAULT 0,
 	FOREIGN KEY (supplier_id) REFERENCES supplier (id)
 );

 CREATE TABLE customer(
 	id int auto_increment PRIMARY KEY,
 	name VARCHAR(255) not null,
 	address VARCHAR(255)
 ); 


 CREATE TABLE orders(
 	id int PRIMARY KEY auto_increment,
 	customer_id int,
 	date TIMESTAMP DEFAULT now(),
 	FOREIGN KEY (customer_id) REFERENCES customer (id)
 );

 CREATE TABLE orders_items(
 	orders_id int,
 	product_id int,
 	amount int DEFAULT 0,
 	discount int DEFAULT 0,
 	PRIMARY KEY(orders_id, product_id),
 	FOREIGN KEY (orders_id) REFERENCES orders (id),
 	FOREIGN KEY (product_id) REFERENCES product (id)
 );

 CREATE TABLE department(
 	id int PRIMARY KEY auto_increment,
 	name VARCHAR(255) not null,
 	budget DOUBLE DEFAULT 0
 );

CREATE TABLE employee(
	id int auto_increment PRIMARY KEY,
	name VARCHAR(255) not null,
	address VARCHAR(255),
	salary DOUBLE DEFAULT 0,
	department_id int,
	FOREIGN KEY (department_id) REFERENCES department (id)
); 


CREATE TABLE employee_orders(
	employee_id int,
	orders_id int,
	FOREIGN KEY (employee_id) REFERENCES employee (id),
	FOREIGN KEY (orders_id) REFERENCES orders (id)
);
