// // ข้อ1
// const koa = require('koa')
// const app = new koa()
// const render = require('koa-ejs');
// const path = require('path');

// render(app, {
//     root: path.join(__dirname, 'views'),    //folder ไหน
//     layout: 'homework10_1',       //ให้ render ไฟล์ไหน
//     viewExt: 'ejs'              // นามสกุลไฟล์
//     ,
//     cache: false,
//     debug: true
// });

// const mysql = require('mysql2');
// const connection = mysql.createConnection({
//     host: 'localhost', user: 'root', password: 'password', database: 'codecamp'
// });

// connection.query(
//     'SELECT * FROM user',
//     function (err, fields) {
//         // console.log(err)
//         app.use(async (ctx, next) => {
//             try {
//                 await ctx.render('homework10_1', {"data":fields});
//                 await next();
//             } catch (err) {
//                 ctx.status = 400
//                 ctx.body = `Uh-oh: ${err.message}`
//                 console.log('Error handle:', err.message)
//             }
//         });
//         console.log(fields); // results contains rows returned by server
//         // console.log(err); // results contains rows returned by server
//     }
// );

// app.listen(3000)
// // // app.use(logger())

// -------------------------------------------------------------------------

const koa = require('koa')
const app = new koa()
const render = require('koa-ejs');
const path = require('path');
const { db } = require('./lib/db.js')
require('./controllers/routes.js')(app);

render(app, {
    root: path.join(__dirname, 'views'),    //folder ไหน
    layout: 'homework10_1',       //ให้ render ไฟล์ไหน
    viewExt: 'ejs',              // นามสกุลไฟล์
    cache: false,
    debug: true
});


// app.use(async (ctx, next) => {
//     try {
//         let results = await db.execute("SELECT * FROM user");
//         await ctx.render('homework10_1', {"data":results})
//         console.log(results)
//         // await next();
//     } catch (err) {
//         ctx.status = 400
//         ctx.body = `Uh-oh: ${err.message}`
//         console.log('Error handle:', err.message)
//     }
// });

app.listen(3000)
// // app.use(logger())

