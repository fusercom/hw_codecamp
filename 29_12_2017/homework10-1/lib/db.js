const { data } = require('../config/database.js')
class Database {
    constructor(){
        this.host = data.host
        this.user = data.user
        this.password = data.password
        this.database = data.database
        this.connectDatabase();
    }
    async connectDatabase(){
        const mysql = require('mysql2/promise');
        this.connection = await mysql.createConnection({host: this.host, user: this.user , password: this.password, database: this.database});
        
    }
    async execute(sql){
        //execute
        const[rows, fields] = await this.connection.execute(sql);
        return rows
    }
}


module.exports.db = new Database();