// const { db } = require('../lib/db.js')
const  {executeUser}  = require('../models/homework10_1.js')

module.exports = function(app){
    ////////////////////
    app.use(async (ctx, next) => {
        const results = await executeUser("SELECT * FROM user");
        // console.log(results)
        await ctx.render('homework10_1', {"data":results})
        await next();
    });
    /////////////////////
}