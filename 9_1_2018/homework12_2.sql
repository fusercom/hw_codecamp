 insert into students (name) VALUES
 	('James Walker'),
 	('Jimmy Luo'),
 	('Mars Well'),
 	('John Dou'),
 	('Mike Hardy'),
 	('Louis Emma'),
 	('Brew Bua'),
 	('Anna Litter'),
 	('Zebra Shraw'),
 	('Mulan Herris');



insert into enrolls (student_id, course_id) VALUES
	(1, 2),
	(2, 4),
	(3, 7),
	(4, 11),
	(5, 18),
	(6, 1),
	(7, 3),
	(8, 4),
	(9, 9),
	(10, 20);


SELECT DISTINCT(course_id) FROM enrolls;


-- SELECT DISTINCT(course_id) FROM courses LEFT JOIN enrolls on course_id = courses.id where student_id is not null;SELECT DISTINCT(course_id) FROM courses LEFT JOIN enrolls on course_id = courses.id where student_id is not null;

SELECT courses.id, courses.name, student_id FROM courses LEFT JOIN enrolls on course_id = courses.id where student_id is null;