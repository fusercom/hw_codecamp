-- use employees;

-- SELECT employees.emp_no, employees.first_name, employees.last_name, titles.title FROM employees LEFT JOIN titles on titles.emp_no = employees.emp_no where title = "manager";

SELECT employees.emp_no, employees.first_name, titles.title, salaries.salary FROM employees 
LEFT JOIN titles on titles.emp_no = employees.emp_no 
LEFT JOIN salaries on salaries.salary where salaries.salary > "manager" LIMIT 100;
