SELECT instructors.name FROM instructors 
left JOIN courses on courses.teach_by = instructors.id where courses.name is null; 

SELECT c.id, c.name as courses_name, i.name FROM courses as c
LEFT JOIN instructors as i on i.id = c.teach_by where c.teach_by is null;