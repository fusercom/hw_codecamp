let fs = require('fs')

var head = new Promise(function(resolve, reject){
    fs.readFile('head.txt', 'utf8', function(err, dataHead){
        if (err)
            reject(err);
        else
            resolve(dataHead);
    })
});
var body = new Promise(function(resolve, reject){
    fs.readFile('body.txt', 'utf8', function(err, dataBody){
        if (err)
            reject(err);
        else
            resolve(dataBody);
    })
});
var feet = new Promise(function(resolve, reject){
    fs.readFile('feet.txt', 'utf8', function(err, dataFeet){
        if (err)
            reject(err);
        else
            resolve(dataFeet);
    })
});
var leg = new Promise(function(resolve, reject){
    fs.readFile('leg.txt', 'utf8', function(err, dataLeg){
        if (err)
            reject(err);
        else
            resolve(dataLeg);
    })
});

Promise.all([head, body, feet, leg])
.then(function(result){
    let text = result.join("\n")
    fs.writeFile('robo.txt', text, 'utf8', function(err){
        if(err)
            console.log(err)
        else
            console.log("Write Success")
    })
})
.catch(function(error){
    console.error(error);
});