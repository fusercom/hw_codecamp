let fs = require('fs')
function writeText1(result){
    return new Promise(function(resolve, reject){
        fs.writeFile('robo.txt', result,'utf8', function(err){
            if (err)
                reject(err);
            else
                resolve();
        })
    })
}

function readHead(){
    return new Promise(function(resolve, reject){
        fs.readFile('head.txt', 'utf8', function(err, dataHead){
            if (err)
                reject(err);
            else
                resolve(dataHead);
        })
    })
}

function readBody(){
    return new Promise(function(resolve, reject){
        fs.readFile('body.txt', 'utf8', function(err, dataBody){
            if (err)
                reject(err);
            else
                resolve(dataBody);
        })
    })
}

function readFeet(){
    return new Promise(function(resolve, reject){
        fs.readFile('feet.txt', 'utf8', function(err, dataFeet){
            if (err)
                reject(err);
            else
                resolve(dataFeet);
        })
    })
}

function readLeg(){
    return new Promise(function(resolve, reject){
        fs.readFile('leg.txt', 'utf8', function(err, dataLeg){
            if (err)
                reject(err);
            else
                resolve(dataLeg);
        })
    })
}

async function roboFile(){
    try {
        let head = await readHead();
        let body = await readBody();
        let feet = await readFeet();
        let leg = await readLeg();
        let result = head + "\n" + body + "\n" + feet + "\n" + leg 
        await writeText1(result);
    } catch (error) {
        console.error(error)
        
    }
}
roboFile();