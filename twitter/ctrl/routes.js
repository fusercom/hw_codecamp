'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')

const pool = require('../lib/db.js')

// const makeTodoCtrl = require('./ctrl/todo')
// const todoRepo = require('./repo/todo')
// const users = makeTodoCtrl(pool, todoRepo)

const router = new Router()
// Auth
    .post('/auth/signup')
    .post('/auth/signin')
    .get('/auth/signout')
    .patch('/auth/verify')

// Upload
    .post('/upload')

// User
    .patch('/user/:id')
    .put('/user/:id/follow')
    .delete('/user/:id/follow')
    .get('/user/:id/follow')
    .get('/user/:id/followd')

// Tweet
    .get('/tweet')
    .post('/tweet')
    .put('/tweet/:id/like')
    .delete('/tweet/:id/like')
    .post('/tweet/:id/retweet')
    .put('/tweet/:id/vote/:voteId')
    .post('/tweet/:id/reply')


// Notification
    .get('/notification')


// Direct Message
    .get('/message')
    .get('/message/:userId')
    .post('/message/:userId')

const app = new Koa()

app.use(bodyParser())
app.use(router.routes())
// app.use(async (ctx, next) => {
//     try {
//         await next()
//     } catch (error) {
//         ctx.status = 400
//         ctx.body = `Uj-jo ${err.message}`
//         console.log(`error`)
//     }
// })


//middle ware
app.use(async function requestLogger (ctx, next) {
    console.log(`${ctx.method} ${ctx.path}`)
    await next()
})

app.listen(3000)