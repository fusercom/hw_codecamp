

// เรียกใช้ library
let assert = require('assert');
let fs = require('fs')


//test 1 function  Assert .deepEqual
// describe('Array', function(){
//     describe('#testObject()', function(){
//         it('should be equal to this object structure', function(){
//             let myVariable = {a:1,b:{c:[1,2,3]}};
//             assert.deepEqual(myVariable, {a:1,b :{c:[1,2,9]}}, 'Obj myVariable');
//         });
//     });
// });

//แบบที่1
function readFile(name, callback){
    fs.readFile(name, 'utf8', function(err, dataDemo){
        if(err)
        callback(err, null)
        else
        callback(null, dataDemo)
    })
}


//แบบที่2
function readFile(name, callback){
    fs.readFile(name, 'utf8', callback)
}



//test 2 function Async
describe('ReadFile', function() {       //กำหนดชื่อ ที่เราจะ test เพื่อให้เรารู็
    describe('readFile', function() {   // กำหนดชื่อ หรือ เรียกใช้ function
        //แบบใช้ callback อันที่1
        it('should be able to read file', done => {   //ใน it นี้เป็นการบอกว่าจะให้มันทำอะไร
            fs.readFile('demo.txt', function(err, data) {
                if (err) {
                    done(err)
                } else {
                    assert.equal(data, 'Hello World')
                    done()
                }
            })
        })
        
        it('should be able to read file demo2', function(done){
            readFile('demo3.txt', function(err, data2){
                if (err)
                done(error)
                else
                assert.deepEqual(data2, "Good bye")
                done()
            })
        })

        //แบบใช้ callback อันที่2
        it('should be able to read file demo', function(done){
            readFile('demo.txt', function(err, data1){
                assert.deepEqual(data1, "Hello World")
                done()
               
            })
        })
    })
})


