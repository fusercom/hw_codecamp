let assert = require('assert');
let myFile = require('./myAdd.js');
let calPlus = myFile.plus;

describe('NumberPlus', function(){
    describe('myAdd',function(){
        it('Should be able calculate 1+1=2', function(){
            assert.equal(calPlus(1,1), 2);
        });
        it('Should be able calculate 2+2=4', function(){
            assert.equal(calPlus(2,3), 5);
        });
    });
});