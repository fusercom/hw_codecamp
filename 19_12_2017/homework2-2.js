let fields = ['id','firstname','lastname','company','salary'];
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];
let arr = []                                        //ประตัวแปร array
for (let i=0; i < employees.length; i++){          //loop สมาชิก ของ employee ออกมาก่อน
    // console.log(employees[i])                       
    let obj = {}                                    //ประตัวแปร obj
    for(let j in fields){                           //loop สมาชิก ของ fields มาเก็บที่ตัวแปร j
        // console.log(fields[j])
        obj[fields[j]] = employees[i][j]            // loop โดย assign ค่า key:value ตามลำดับ
        // console.log(obj)
    }
    arr.push(obj)                                   //assign obj ที่เก็บค่า key:value เข้าไปใน array
}
console.log(arr)