let {Employee} = require('./employee.js')


class Programmer extends Employee{
    constructor(id, firstname, lastname, salary, role, type) {
        super(firstname, lastname, salary);
        this.id = id;
        this.type = type;
        this.role = role;
    }
    work(){
        this._CreateWebsite()
        this._FixPC()
        this._InstallWindows()
    }
    _CreateWebsite(){
        console.log(this.firstname + " is programmer to create website")
    }
    _FixPC(){
        console.log(this.firstname + " is programmer to fix pc.")
    }
    _InstallWindows(){
        console.log(this.firstname + " is programmer to install windows")
    }
}


exports.Programmer = Programmer