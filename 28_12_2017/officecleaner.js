let {Employee} = require('./employee.js')

class OfficeCleaner extends Employee{
    constructor(id, firstname, lastname, salary, role, dressCode){
        super(firstname, lastname, salary)
        this.id = id
        this.role = role
        this.dressCode = dressCode
    }
    work(){
        this.clean()
        this.killCoachroach()
        this.decorateRoom()
        this.welcomeGuest()
    }
    clean(){
        console.log(this.firstname + " is OfficeCleanner to clean.")
    }
    killCoachroach(){
        console.log(this.firstname + " is OfficeCleanner to killCoachroach.")
    }
    decorateRoom(){
        console.log(this.firstname + " is OfficeCleanner to decorateRoom.")
    }
    welcomeGuest(){
        console.log(this.firstname + " is OfficeCleanner to welcomeGuest.")
    }
}

exports.OfficeCleaner = OfficeCleaner