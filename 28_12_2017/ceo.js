let EventEmitter = new(require('events'))
let {Programmer} = require('./programmer')
let {Employee} = require('./employee.js')
let {OfficeCleaner} = require('./officecleaner.js')
let fs = require('fs')

class CEO extends Employee {
    constructor(id, firstname, lastname, salary, role, dressCode) {
        super(firstname, lastname, salary);
        this.id = id  
        this.role = role
        this.dressCode = dressCode;      
        this.employeesRaw = []
        this.employees = []
        let self = this 
    }
    
    async readFile(){
        let self = this
        try {
            return await new Promise(function(resolve, reject){
                fs.readFile('employees.json','utf8', function(err, data) {
                    let employees = [];
                    if(err)
                        reject(err)
                    else {
                        self.employeesRaw = JSON.parse(data)
                        for (let i=0; i<self.employeesRaw.length;i++){
                            if (self.employeesRaw[i].role == "Programmer"){
                                employees[i] = new Programmer(
                                    self.employeesRaw[i].id,                   
                                    self.employeesRaw[i].firstname,
                                    self.employeesRaw[i].lastname,
                                    self.employeesRaw[i].salary,
                                    self.employeesRaw[i].role,             
                                    self.employeesRaw[i].type)                   
                            }
                            else if (self.employeesRaw[i].role == "OfficeCleaner"){
                                employees[i] = new OfficeCleaner(
                                    self.employeesRaw[i].id,                   
                                    self.employeesRaw[i].firstname,
                                    self.employeesRaw[i].lastname,
                                    self.employeesRaw[i].salary,
                                    self.employeesRaw[i].role,                   
                                    self.employeesRaw[i].dressCode)                   
                            }
                            else if(self.employeesRaw[i].role == "CEO"){
                                employees[i] = new CEO(
                                    self.employeesRaw[i].id,                   
                                    self.employeesRaw[i].firstname,
                                    self.employeesRaw[i].lastname,
                                    self.employeesRaw[i].salary,
                                    self.employeesRaw[i].role,                   
                                    self.employeesRaw[i].dressCode)     
                            }
                        }
                        self.employees = employees;
                        resolve(self.employees)
                    }
                })    
            })
        } 
        catch (error) {
            console.log(error)
        }
    }

    talk(message){
        console.log(message)
    }
    reportRobot(self,robotMessage){
        self.talk(robotMessage)
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        employee.setSalary(newSalary)
    }
    gossip(employee){
        console.log("Hey " + employee.firstname + " Today is very cold!")
    }
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    };
    _fire(employee){
        this.dressCode = 'tshirt'
        console.log(employee.firstname + " has been fired! Dress with :" + this.dressCode);
    }
    _hire(employee){
        this.dressCode = 'tshirt'
        console.log(employee.firstname + " has been hired back! :" + this.dressCode);
    }
    _seminar(){
        this.dressCode = 'suit'
        console.log("He is going to seminar Dress with :" + this.dressCode);
    }
    
}


exports.CEO = CEO