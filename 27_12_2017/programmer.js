let {Employee} = require('./employee.js')


class Programmer extends Employee{
    constructor(firstname, lastname, salary, id, type="Programmer") {
        super(firstname, lastname, salary);
        this.id = id;
        this.type = type;
    }
    work(){
        this._CreateWebsite()
        this._FixPC()
        this._InstallWindows()
    }
    _CreateWebsite(){
        console.log()
    }
    _FixPC(){
        console.log()
    }
    _InstallWindows(){
        console.log()
    }
}


exports.Programmer = Programmer