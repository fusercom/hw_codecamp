let {Employee} = require('./employee.js')
let {CEO} = require('./ceo.js')


let somchai = new CEO("Somchai","Sudlor",30000);
let somsri = new Employee("Somsri","Sudsuay",22000);
somchai.gossip(somsri);
somchai._fire(somsri);
somchai._hire(somsri);
somchai._seminar(somsri);
somchai._golf(somsri);
// console.log(setSalary(30000))


somchai.increaseSalary(somsri, 20);
somchai.increaseSalary(somsri, 25000);