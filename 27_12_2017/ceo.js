let {Programmer} = require('./programmer')
let {Employee} = require('./employee.js')
let fs = require('fs')

class CEO extends Employee {
    constructor(firstname, lastname, salary, dressCode) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';        
        this.employeesRaw = []
        this.employees = []
    }
    
    async readFile(){
        let self = this
        try {
            await new Promise(function(resolve, reject){
                fs.readFile('homework1.json','utf8', function(err, data) {
                    let employees = [];
                    if(err)
                        reject(err)
                    else {
                        self.employeesRaw = JSON.parse(data)
                        for (let i=0; i<self.employeesRaw.length;i++){
                            employees[i] = new Programmer(
                                self.employeesRaw[i].firstname,
                                self.employeesRaw[i].lastname,
                                self.employeesRaw[i].salary,
                                self.employeesRaw[i].id)                          
                        }
                        self.employees = employees;
                        resolve(data)
                    }
                })    
            })
        } 
        catch (error) {
            console.log(error)
        }
    }


    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        employee.setSalary(newSalary)
    }
    gossip(employee){
        console.log("Hey " + employee.firstname + " Today is very cold!")
    }
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    };
    _fire(employee){
        this.dressCode = 'tshirt'
        console.log(employee.firstname + " has been fired! Dress with :" + this.dressCode);
    }
    _hire(employee){
        this.dressCode = 'tshirt'
        console.log(employee.firstname + " has been hired back! :" + this.dressCode);
    }
    _seminar(){
        console.log("He is going to seminar Dress with :" + this.dressCode);
    }
    
}
exports.CEO = CEO