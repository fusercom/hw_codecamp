SELECT c.name, SUM(c.price)
FROM enrolls as e 
JOIN courses as c on c.id = e.course_id 
GROUP BY c.name;

SELECT s.id, s.name, SUM(c.price)
FROM enrolls as e 
JOIN courses as c on c.id = e.course_id 
JOIN students as s on s.id = e.student_id
GROUP BY s.id;
 