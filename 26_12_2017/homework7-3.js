input1 = [1,2,3]
input2 = {a:1,b:2}
input3 = [1,2,{a:1,b:2}]
input4 = [1,2,{a:1,b:{c:3,d:4}}]


function cloneObj(params){
    let temp;
    if(Array.isArray(params) == true){     // check ว่าเป็นarray หรือไม่
        temp = [];
        for (let key in params) {
            if(typeof(params[key]) == 'number'){      
                temp[key] = params[key];
            }
            else{
                temp[key] = {}
                for (let key2 in params[key]){
                    if (typeof(params[key][key2] == 'number')){
                        temp[key][key2] = params[key][key2];
                    }
                    else{
                        temp[key][key2] = {}
                        for(let key3 in params[key][key2]){
                            if(typeof(params[key][key2][key3] == 'number')){
                                temp[key][key2][key3] = params[key][key2][key3];
                            }
                        }
                    }
                }
            }
        }
    }
    else {
        temp = {};
        for (let key in params) {
            temp[key] = params[key];
        }
    }
    return temp;
}




let i1 = cloneObj(input1)
let i2 = cloneObj(input2)
let i3 = cloneObj(input3)
let i4 = cloneObj(input4)
i1[1] = 9
i2['a'] = 9
i3[2]['a'] = 9
i4[2]['b']['d'] = 30
console.log(input1)
console.log(i1)
console.log('----------------------------------')
console.log(input2)
console.log(i2)
console.log('----------------------------------')
console.log(input3)
console.log(i3)
console.log('----------------------------------')
console.log(input4)
console.log(i4)

exports.cloneObj = cloneObj