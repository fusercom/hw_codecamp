let fs = require('fs')

function readFile(){
    return new Promise(function(resolve, reject){
        fs.readFile('homework1.json', 'utf8', function(err, data){
            let content = JSON.parse(data)
            if(err)
                reject(err)
            else{
                resolve(content)
            }
        })
    })
}

function addYearSalary(row){
    row.yearSalary = parseInt(row['salary']) * 12
    return row
}


function addNextSalary(row){
    let nextSalary = []
    nextSalary[0] = parseInt(row['salary']) * 1.1
    nextSalary[1] = parseInt(row['salary']) * 1.21
    nextSalary[2] = parseInt(row['salary']) * 1.331
    row.nextSalary = nextSalary
    // console.log(row)
}

function addAdditionalFields(row){
    row.map((x)=>{
        addNextSalary(x)
        addYearSalary(x)
    })
}


async function runFile(){
    try {
        let employees = await readFile()
        let person = await addAdditionalFields(employees)
        console.log(employees)
    } catch (error) {
        console.error(error)
    }
}
runFile()