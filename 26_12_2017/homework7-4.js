let assert = require('assert')
let clone = require('./homework7-3.js')
let cloneData = clone.cloneObj

input1 = [1,2,3]
input2 = {a:1,b:2}
input3 = [1,2,{a:1,b:2}]
input4 = [1,2,{a:1,b:{c:3,d:4}}]


i1[1] = 9
i2['a'] = 9
i3[2]['a'] = 9
i4[2]['b']['d'] = 30


describe('CheckData', function(){
    describe('Check', function(){
        it('It should be able work', function() {
            assert.deepEqual(cloneData(i1), input1, 'fails')
        })
    })
})