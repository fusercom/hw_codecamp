const db = require('../config/database.json')
const mysql = require('mysql2/promise')
const pool = mysql.createPool(db)
const makeTodoCtrl = require('./../ctrl/booklist')
const booklistModel = require('./../model/booklist')
const booklistCtrl = makeTodoCtrl(pool, booklistModel)
const Router = require('koa-router')

const router = new Router()
// console.log(router)
    .get('/booklist', booklistCtrl.list)
    .post('/booklist', booklistCtrl.add)
    .delete('/booklist/:id', booklistCtrl.remove)
    .put('/booklist/:id', booklistCtrl.update)



module.exports = router

// app.use(async(ctx, next) => {
//     console.log(ctx.path)
//     await next()
// })


