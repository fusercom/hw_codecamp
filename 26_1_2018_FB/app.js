const Koa = require('koa')
const bodyParser = require('koa-bodyparser')



const app = new Koa()
const router = require('./ctrl/route.js')
const cors = require('koa-cors');

app.use(bodyParser())
app.use(cors())
app.use(router.routes())
app.listen(3000)