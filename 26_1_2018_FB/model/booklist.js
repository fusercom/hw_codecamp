module.exports = {
    list,
    add,
    remove,
    update,
}

async function list(db) {
    const [rows] = await db.execute(`select id, title, price, promotion_date, image from booklist`)
    console.log(rows)
    return rows
}

async function add(db, booklist) {
    let [rows] = await db.execute(`insert into booklist (title, price, promotion_date, image) values (?,?,?,?)`, [booklist.title, booklist.price, booklist.promotion_date, booklist.image])
    booklist.id = rows.insertId
    if (rows.insertId) {
        return {
            id: booklist.id,
            title: booklist.title,
            price: booklist.price,
            promotion_date: booklist.promotion_date,
            image: booklist.image
        }
    }  else {
        return 'Error'
      }
}

async function remove(db, id) {
    await db.execute(`delete from booklist where id = ? `, [id])
}


async function update(db, id, content) {
    return await db.execute(`update booklist set title = ?, price = ? where id = ?`, [content.title, content.price, id])
}