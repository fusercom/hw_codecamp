module.exports = function (pool, repo) {
  return {
    async list (ctx) {
      ctx.body = await repo.list(pool)
    },
    async create (ctx) {
      const todo = ctx.request.body
      // TODO: validate todo
      const obj = await repo.create(pool, todo)
      ctx.body = obj
    },
    async get (ctx) {
      const id = ctx.params.id
      const findId = await repo.find(pool, id)
      ctx.body = findId
    },
    async update (ctx) {
      const id = ctx.params.id
      const content = ctx.request.body
      const update = await repo.changeContent(pool, id, content)
      ctx.body = update
    },
    async remove (ctx) {
      const id = ctx.params.id
      ctx.body = await repo.remove(pool, id)
    },
    async complete (ctx) {
      const id = ctx.params.id
      ctx.body = await repo.markComplete(pool, id)
    },
    async incomplete (ctx) {
      const id = ctx.params.id
      ctx.body = await repo.markIncomplete(pool, id)
    },
  }
}
