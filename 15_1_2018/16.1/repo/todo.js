module.exports = {
  create,
  list,
  find,
  changeContent,
  remove,
  markComplete,
  markIncomplete
}

async function create (db, todo, ) {
  const [rows] = await db.execute(`insert into todo (name, status) values (?,?)`, [todo.name, todo.status])
  todo.id = rows.insertId
  if (rows.insertId) {
    return { 
      id: todo.id, 
      name: todo.name, 
      status: todo.status 
    }
  } else {
    return 'Error'
  }
}

async function list (db) {
  const [rows] = await db.execute(`select id, name, status from todo`)
  return [rows]
}

async function find (db, id) {
  const [rows] = await db.execute(`select id, name, status from todo where id = ?`, [id])
  return rows[0]
}

async function changeContent (db, id, content) {
  return await db.execute(`update todo set name = ?, status = ? where id = ?`, [content.name, content.status, id])
}

async function remove (db, id) {
  await db.execute(`delete from todo where id = ?`, [id])
}

async function markComplete (db, id) {
  await db.execute(`update todo set status = 1 where id = ?`, [id])
  return await find(db, id)
}

async function markIncomplete (db, id) {
  await db.execute(`update todo set status = 0 where id = ?`, [id]) 
  return await find(db, id)
}
